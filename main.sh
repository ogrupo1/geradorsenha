#!/bin/bash

source ./funcoes.sh

while true; do
	#variável que realiza os testes
	testador=0
	
	#salva as opções da senha
	configs=$(menu)
	testador=$? #salva a escolha de gerar senha ou cancelar
	
	#testa se o user escolheu limpar o arquivo, caso positivo, ele inciará a função de limpar e sairá	
	test $testador = 2 && limpa && break

	#testa se o user escolheu sair, caso contrario salva as configuracoes
	test $testador = 1 && break || IFS="|" read -ra rconfigs <<< "$configs"
	
	#salva o tamanho da senha
	testador=${rconfigs[1]}
	
	#testa se o tamanho da senha e nulo e, caso seja, lanca o erro de tamanho
	test $testador = 0 && erro_tam && break
	
	#tenta gerar a senha, levantando erro caso nenhum tipo de caracter tenha sido utilizado	
	s1=$(geradorLM "${rconfigs[@]}")
	
	#verifica se conseguiu gerar a senha e, caso positivo gera as demais senhas, caso contrário para o script
	test -n "$s1" && s2=$(geradorLM "${rconfigs[@]}") && s3=$(geradorLM "${rconfigs[@]}") || break	
	
	#exibe os resultados gerados
	null=$(gerados $s1 $s2 $s3)
	
	#salva a opção escolhida
	esc=$?	
	
	#para, salva ou continua, baseada na opção escolhida
	if [ $esc = 1 ]; then
		break
	elif [ $esc = 3 ]; then
		echo $s1 >> senhas_geradas.txt
		echo $s2 >> senhas_geradas.txt
		echo $s3 >> senhas_geradas.txt

		yad --width=250 --height=50 --undecorated \
			--center --on-top \
			--buttons-layout=center \
			--text='Senhas salvas!' \
			--text-align=center \
			--button=OK:0

		yad --width=250 --height=100 --undecorated \
			--center --on-top \
			--buttons-layout=center \
			--text='Gerar novas senhas?' \
			--text-align=center \
			--button=SIM:0 \
			--button=NÃO:1
	fi
	
	#verifica se o user quer gerar outra senha
	test $? = 1 && break

done
