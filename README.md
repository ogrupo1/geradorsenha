# GeradorSenha

Este script gera senhas com tamanho e tipos de caracteres escolhidos pelo usuário e salva-os num arquivo chamado 'senhas_geradas.txt', ele também pode limpar esse arquivo gerado.

Para usá-lo utilize *'bash main.sh'* e isso fará com que ele abra uma interface gráfica que pedirá para o usuário coloque as opções de senha ou escolha a opção de limpar.

As senhas geradas serão armazenadas no arquivo já citado onde ficarão armazenadas até que o usuário limpe o arquivo.
