#!/bin/bash

#funcao de menu que permite escolher as opcoes da senha 
function menu(){
	yad --form --height=300 --center \
		--title="GERADOR DE SENHAS" \
		--field="PERSONALIZANDO A SENHA":LBL \
		--field="Escolha o tamanho da senha":NUM \
		--field="":LBL \
		--field="Escolha os caracteres da senha:":LBL \
		--field="Simbolos":CHK \
		--field="Letras Maiusculas":CHK \
		--field="Letras Minusculas":CHK \
		--field="Numeros":CHK \
		--button='Limpar':2 \
		--button='Cancelar':1 \
		--button='Gerar!':0 
}

#função que gera senha
function geradorLM(){
	#garante que o tamanho da senha não seja nulo
	tam=$(echo $2)

	#converte os valores booleanos obtidos no menu em 0 e 1 
        sim=$(test "$5" = "TRUE" && echo 1 || echo 0 )
        mai=$(test "$6" = "TRUE" && echo 1 || echo 0 )
        min=$(test "$7" = "TRUE" && echo 1 || echo 0 )
        num=$(test "$8" = "TRUE" && echo 1 || echo 0 )
	
	#echo $tam / $sim / $mai / $min / $num

	#garante que ao menos um tipo de caracter foi escolhido e finaliza o script caso contrário
        test $sim -eq 0 && test $mai -eq 0 && test $min = 0 && test $num = 0 \
						&& yad --title='ERRO!' --width=300 --height=50 --center --on-top \
       							--text='É preciso escolher ao menos um tipo de caracter!' \
							--text-align=center \
							--button=OK:0 && exit
	
	#finaliza o script caso tenha disparado o erro anterior
        ger=$?
	test $ger = 0 && exit

	#gera a senha
        for ((i=0; i!=$tam ;i++)); do
                #gera um simbolo que seja numero, simbolo ou letra maiuscula e minuscula
                valor=$(python3 -c "from random import randint; print(randint(33,126))")
		
                #verifica se o user NÃO quer simbolo
                if [ $sim = 0 ]; then
                        #caso positivo, verifica se o valor gerado não é correspondente a um simbolo na tabela ascii, caso seja remove o valor da variável
                        if [ $valor -le 47 ] || [ $valor -gt 57 ] && [ $valor -le 64 ] || [ $valor -ge 91 ] && [ $valor -le 96 ] || [ $valor -gt 122 ]; then
                                valor=''
                        fi
                fi

                #verifica se o user NÂO quer uma letra MAIÚSCULA
                if [ $mai = 0 ]; then
                        #caso positivo, verifica se o valor gerado não é correspondente a uma letra maiúscula na tabela ascii, caso seja remove o valor da variável
                        test "$valor" != "" && if [ $valor -ge 65 ] && [ $valor -le 90 ]; then
                                valor=''

                        fi
                fi

                #verifica se o user NÂO quer uma letra MINÚSCULA
                if [ $min = 0 ]; then
                        #caso positivo, verifica se o valor gerado não é correspondente a uma letra minúscula na tabela ascii, caso seja remove o valor da variável
                        test "$valor" != "" && if [ $valor -ge 97 ] && [ $valor -le 122 ]; then
                                valor=''
                        fi
                fi

                #verifica se o user NÂO quer um número
                if [ $num = 0 ]; then
                        #caso positivo, verifica se o valor gerado não é correspondente a um número na tabela ascii, caso seja remove o valor da variável
                        test "$valor" != "" && if [ $valor -ge 48 ] && [ $valor -le 57 ]; then
                                valor=''
                        fi
                fi
		
		#verifica se o valor atendeu os tipos de caracteres escolhidos. Caso negativo, força gerar um valor novo e, caso positivo, converte ele no caracter
                if [ "$valor" = "" ]; then
                        i=$(( i - 1))
		else	
			#verifica se o valor gerado é igual a 38, que na tabela ascii é referente ao &, caso positivo ele adiciona '&amp;' à senha para evitar erros
			if [ $valor = 38 ]; then
				senha+='&amp;'
			#verifica se o valor gerado é igual a 22, 39, 60 ou 62 referentes a '"', ''', '<' e '>' na tabela ascii, rescpectivamente, caso positivo nao adiciona à senha para evitar erros
			elif [ $valor = 22 ] || [ $valor = 39 ] || [ $valor = 60 ] || [ $valor = 62 ]; then
				i=$(( i - 1))
			else
				car=$(python3 -c "print(chr($valor))")
				senha+=$car
			fi
                fi
        done
	
	#lança a senha criada
	echo $senha

}

#funcao que exibe as senhas geradas
function gerados(){
	yad 	--form --widgh=200 --height=300 --center \
		--title="RESULTADO" \
		--field="SENHAS GERADAS":LBL \
		--field="Senha 1: '$1'":LBL \
		--field="Senha 2: '$2'":LBL \
		--field="Senha 3: '$3'":LBL \
		--button="Cancelar":1 \
		--button="Gerar nov.":2 \
		--button="Salvar":3
}

#funcao para o erro de tamanho nulo
function erro_tam(){
	yad 	--title='ERRO!' --width=300 --height=50 --center --on-top \
		--text='A senha não pode ter tamanho zero!' \
        	--text-align=center \
        	--button=OK:0
}

#função que apaga todas as senhas salvas
function limpa(){
	yad 	--title='LIMPAR SENHAS' --width=250 --height=100 \
                --center --on-top \
                --buttons-layout=center \
		--text='ATENÇÃO' \
                --text='Deseja apagar as senhas salvas?' \
                --text-align=center \
                --button=SIM:1 \
                --button=NÃO:0
	
	esco=$?

	if [ $esco = 1 ]; then
		printf '' > senhas_geradas.txt
		yad 	--undecorated --width=250 --height=50 \
			--center --on-top --buttons-layout=center \
			--text='Arquivo de senhas salvas limpo!' \
			--text-align=center \
			--button=OK:0
	fi
}
